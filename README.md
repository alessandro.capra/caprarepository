# CapraRepository

Dentro questo file ho inserito i comandi più importanti utilizzati a lezione accompagnati da una breve descrizione

## Creazione file/cartelle

1. `mkdir`: Crea una nuova directory (cartella) nel percorso specificato.

2. `touch`: Crea un nuovo file vuoto nel percorso specificato.

## Elenchi (o liste)

3. `ls`: Elenca i file e le directory nella directory corrente.

4. `ls -la`: Elenca tutti i file e le directory, inclusi quelli nascosti, nella directory corrente in un formato dettagliato.

## Lavorare con i branch

5. `git branch`: Visualizza, crea o elimina rami nel repository Git.

6. `git merge`: Unisce due rami Git. È utilizzato per combinare le modifiche di un ramo con un altro.

7. `git checkout`: Consente di spostarsi tra i rami Git o ripristinare i file a una versione precedente.

## Comandi legati ai commit

8. `git commit`: Salva le modifiche nel repository Git. È necessario fornire un messaggio di commit che descriva le modifiche apportate.

9. `git add`: Aggiunge file modificati o nuovi al "staging area" per il commit.

10. `git log`: Visualizza la cronologia dei commit nel repository.

11. `git init`: Inizializza un nuovo repository Git nella directory corrente.

## Comandi per lavorare con le repository

12. `git push`: Carica i commit locali sul repository remoto.

13. `git pull`: Scarica i commit dal repository remoto e li integra nel ramo locale.

14. `git clone`: Crea una copia di un repository remoto sul tuo computer.

## Comandi che migliorano l'usabilità di git

15.  `git tag`: Etichetta specifici commit come versioni nel repository.

16.  `git stash`: Nasconde le modifiche non commesse in uno stack temporaneo (spazio in cui gli sviluppatori possono accantonare le modifiche non ancora definitive), consentendo di lavorare su altre modifiche senza committarle

17.  `git fetch`: Recupera i commit, le referenze dei rami e i file remoti dal repository remoto nel repository locale, ma non li fonde automaticamente con il tuo ramo attuale

18. `git status`: Mostra lo stato attuale del repository Git, inclusi i file modificati, i file non monitorati, ecc.

19. `cd`: Cambia la directory corrente. Viene utilizzato per spostarsi tra le directory nel sistema.

## Collegamenti a editor di testo

20. `nano`: È un semplice editor di testo da terminale. Puoi usarlo per modificare file di testo direttamente da linea di comando.

21. Per accedere al file markdown o di testo, il comando è: code nome_file.md/.txt

## I conflitti

Ora, riguardo ai conflitti: i conflitti vanno evitati in ogni modo possibile perchè principale causa di perdita di tempo e pazienza. Si verificano quando due o più rami hanno modificato la stessa parte di un file in modi diversi, in questo modo Git non è in grado di determinare automaticamente quale versione del file deve essere mantenuta. Per risolvere i conflitti:

1. **Identificazione**: Git ti avvisa dei conflitti durante un'operazione come merge o pull. Puoi visualizzare i file in conflitto per risolverli manualmente.

2. **Risoluzione manuale**: Apri i file in conflitto e modificali manualmente per risolvere le discrepanze. Git inserisce marcatori speciali (<<<<<<<, =======, >>>>>>>) per indicare le sezioni conflittuali. È necessario risolvere manualmente queste sezioni.

3. **Commit delle modifiche**: Una volta risolti i conflitti, esegui `git add` sui file modificati e quindi `git commit` per completare l'operazione di merge o pull.

Ancora più importante è evitare conflitti, per farlo è consigliabile:

- **Aggiornare frequentemente**: Mantieni il tuo repository locale aggiornato con `git pull` per ridurre le possibilità di conflitti.
- **Lavorare su rami separati**: Suddividere il lavoro in rami separati può aiutare a ridurre la possibilità di conflitti, specialmente quando si lavora su funzionalità diverse.
- **Comunicazione**: Comunica con gli altri programmatori il modo in cui intendi modificare il/i file, per evitare sovrapposizioni non necessarie.